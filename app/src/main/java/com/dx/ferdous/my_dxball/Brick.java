package com.dx.ferdous.my_dxball;

/**
 * Created by ferdous on 9/8/17.
 */

public class Brick {

    private float brick_left=0;
    private float brick_right=0;
    private float brick_top=0;
    private float brick_buttom=0;

    private float x_max=0;
    private float y_max=0;

    private float brick_height=0;
    private float brick_width=0;

    private float brick_movespeed=0;

    private boolean move_flag = true;

    private boolean is_alive = true;

    Brick(){

        brick_left=0;
        brick_right=0;
        brick_top=0;
        brick_buttom=0;

        x_max=0;
        y_max=0;

        brick_height=0;
        brick_width=0;

        brick_movespeed=0;

        move_flag=true;

        is_alive = true;

    }

    Brick(float left,float top,float right,float buttom,float x_m,float y_m,float move){

        brick_left=left;
        brick_right=right;
        brick_top=top;
        brick_buttom=buttom;

        x_max=x_m;
        y_max=y_m;

        brick_height=0;
        brick_width=0;

        brick_movespeed=move;

        move_flag=true;

        is_alive = true;


    }



    public void move_brick(){

        if(move_flag){

            if(brick_right>=x_max){

                move_flag=false;
            }

            brick_left+=brick_movespeed;
            brick_right+=brick_movespeed;





        }else{

            if(brick_left<=0){

                move_flag=true;
            }

            brick_left-=brick_movespeed;
            brick_right-=brick_movespeed;
        }



    }

    public boolean collusion(){

       if(collusion_buttom()|| collusion_right()|| collusion_top() || collusion_left()) {

           return true;
       }

        return false;

    }

    public boolean collusion_left(){

        if(Ball.get_bally()+Ball.get_ballradius()>=brick_top && Ball.get_bally()-Ball.get_ballradius()<= brick_buttom && Ball.get_ballx()+Ball.get_ballradius()>=brick_left && Ball.get_ballx()+Ball.get_ballradius()<=brick_right){

            is_alive=false;
            Ball.set_xflag(!Ball.get_xflag());
            return true;



        }
        return false;

    }

    public boolean collusion_right(){

        if(Ball.get_bally()+Ball.get_ballradius()>=brick_top && Ball.get_bally()-Ball.get_ballradius()<= brick_buttom && Ball.get_ballx()-Ball.get_ballradius()<=brick_right && Ball.get_ballx()-Ball.get_ballradius()>=brick_left){

            is_alive=false;
            Ball.set_xflag(!Ball.get_xflag());
            return true;

        }
        return false;

    }

    public boolean collusion_top(){

        if(Ball.get_ballx()+Ball.get_ballradius()>=brick_left && Ball.get_ballx()-Ball.get_ballradius()<= brick_right && Ball.get_bally()+Ball.get_ballradius()>=brick_top && Ball.get_bally()+Ball.get_ballradius()<=brick_buttom ){

            is_alive=false;
            Ball.set_yflag(!Ball.get_yflag());
            return true;

        }
        return false;

    }

    public boolean collusion_buttom(){

        if(Ball.get_ballx()+Ball.get_ballradius()>=brick_left && Ball.get_ballx()-Ball.get_ballradius()<= brick_right && Ball.get_bally()-Ball.get_ballradius()>=brick_buttom && Ball.get_bally()-Ball.get_ballradius()<=brick_top){

            is_alive=false;
            Ball.set_yflag(!Ball.get_yflag());
            return true;

        }

        return false;

    }

    public void set_brick_left(float left){brick_left=left;}
    public void set_brick_top(float top){brick_top=top;}
    public void set_brick_right(float right){brick_right=right;}
    public void set_brick_buttom(float buttom){brick_buttom=buttom;}




    public float get_brick_left(){return brick_left;}
    public float get_brick_top(){return brick_top;}
    public float get_brick_right(){return brick_right;}
    public float get_brick_buttom(){return brick_buttom;}
    public boolean get_status(){return is_alive;}








}
