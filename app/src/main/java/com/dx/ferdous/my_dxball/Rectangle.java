package com.dx.ferdous.my_dxball;

/**
 * Created by ferdous on 9/6/17.
 */

public final class Rectangle {

    private static float rect_left=0;
    private static float rect_top=0;
    private static float rect_right=0;
    private static float rect_buttom=0;

    private static float x_max=0;
    private static float y_max=0;

    private static float height=0;
    private static float width=0;

    private static float bar_movespeed=10;



    Rectangle(){

        rect_left=0;
        rect_top=0;
        rect_right=0;
        rect_buttom=0;
        x_max=0;
        y_max=0;
        height=20;
        width=30;
        bar_movespeed=10;

    }

    Rectangle(float x1,float y1,float x2,float y2){

        rect_left=x1;
        rect_top=y1;
        rect_right=x2;
        rect_buttom=y2;

    }

    public static float get_rectleft(){return rect_left;}

    public static float get_recttop(){return rect_top;}

    public static float get_rectright(){return rect_right;}

    public static float get_rectbuttom(){return rect_buttom;}

    public static float get_height(){return height;}

    public static  void move_left(){

        rect_left-=bar_movespeed;
        rect_right-=bar_movespeed;

    }

    public static  void move_right(){

        rect_left+=bar_movespeed;
        rect_right+=bar_movespeed;

    }

    public static void set_Rect_left(float item){rect_left=item;}

    public static void set_Rect_left(){rect_left=x_max*35/100;}

    public static void set_Rect_top(float item){rect_top=item;}
    public static void set_Rect_top(){rect_top=y_max-height;}

    public static void set_Rect_right(float item){rect_right=item;}
    public static void set_Rect_right(){rect_right=get_rectleft()+(x_max/100)*width;}

    public static void set_Rect_buttom(float item){rect_buttom=item;}
    public static void set_Rect_buttom(){rect_buttom=y_max;}

    public static void set_xmax(float item){x_max=item;}

    public static void set_ymax(float item){y_max=item;}






}
