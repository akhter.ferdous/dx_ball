package com.dx.ferdous.my_dxball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by ferdous on 9/3/17.
 */

public class GameManager extends View {


    Paint paint;
    float x_max = 0, y_max = 0;
    float tx=0,ty=0;
    boolean firstTime = true;

    int score = 0, life = 3;

    Brick ob1,ob2,ob3,ob4 ;



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (firstTime) {
            firstTime = false;
            x_max = canvas.getWidth();
            y_max = canvas.getHeight();

            Rectangle.set_xmax(x_max);
            Rectangle.set_ymax(y_max);
            Rectangle.set_Rect_left(x_max*35/100);
            Rectangle.set_Rect_top(y_max-20);
            Rectangle.set_Rect_right(x_max*65/100);
            //Rectangle.set_Rect_right(Rectangle.get_rectleft()+(x_max/100)*30);
            Rectangle.set_Rect_buttom(y_max);


            Ball.set_xmax(x_max);
            Ball.set_ymax(y_max);
            Ball.set_ballradius(30);
            Ball.set_ballx((Rectangle.get_rectleft()+Rectangle.get_rectright())/2);
            Ball.set_bally(y_max-Ball.get_ballradius()-Rectangle.get_height()-30);

            Ball.set_ballspeed(5);

            ob1 = new Brick((x_max*4)/100,(y_max*10)/100,(x_max*24)/100,(y_max*20)/100,x_max,y_max,0);
            ob2 = new Brick((x_max*28)/100,(y_max*10)/100,(x_max*48)/100,(y_max*20)/100,x_max,y_max,0);
            ob3 = new Brick((x_max*52)/100,(y_max*10)/100,(x_max*72)/100,(y_max*20)/100,x_max,y_max,0);
            ob4 = new Brick((x_max*76)/100,(y_max*10)/100,(x_max*96)/100,(y_max*20)/100,x_max,y_max,0);

        }


        canvas.drawRGB(255, 255, 255);

        //text
        paint.setColor(Color.YELLOW);
        paint.setTextSize(40);
        //paint.setFakeBoldText(true);
        canvas.drawText("SCORE: "+String.valueOf(score),10,40,paint);

        paint.setTextSize(40);
        //paint.setFakeBoldText(true);
        canvas.drawText("LIFE: "+String.valueOf(life),x_max-150,40,paint);


        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(Ball.get_ballx(),Ball.get_bally() , Ball.get_ballradius(), paint);
        Ball.ballMovement();

        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawRect(Rectangle.get_rectleft(), Rectangle.get_recttop(),Rectangle.get_rectright(),Rectangle.get_rectbuttom() , paint);

        if(ob1.get_status()){

            paint.setColor(Color.GREEN);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(ob1.get_brick_left(),ob1.get_brick_top(),ob1.get_brick_right(),ob1.get_brick_buttom(),paint);
            ob1.move_brick();
            if(ob1.collusion()){

                score+=10;
            }
        }

        if(ob2.get_status()){

            paint.setColor(Color.DKGRAY);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(ob2.get_brick_left(),ob2.get_brick_top(),ob2.get_brick_right(),ob2.get_brick_buttom(),paint);
            ob2.move_brick();
            if(ob2.collusion()){

                score+=10;
            }
        }

        if(ob3.get_status()){

            paint.setColor(Color.GREEN);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(ob3.get_brick_left(),ob3.get_brick_top(),ob3.get_brick_right(),ob3.get_brick_buttom(),paint);
            ob3.move_brick();
            if(ob3.collusion()){

                score+=10;
            }
        }

        if(ob4.get_status()){

            paint.setColor(Color.DKGRAY);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(ob4.get_brick_left(),ob4.get_brick_top(),ob4.get_brick_right(),ob4.get_brick_buttom(),paint);
            ob4.move_brick();
            if(ob4.collusion()){

                score+=10;
            }
        }


        invalidate();
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {

        tx = event.getX();
        ty = event.getY();

        if(tx<Rectangle.get_rectleft() && tx>0){

            Rectangle.move_left();
            return true;
        }

        if(tx>Rectangle.get_rectright() && tx<x_max){

            Rectangle.move_right();
            return true;
        }




        return super.onTouchEvent(event);


    }

    public GameManager(Context context) {
        super(context);
        paint = new Paint();
    }
}