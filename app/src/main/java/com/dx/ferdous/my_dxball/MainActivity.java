package com.dx.ferdous.my_dxball;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnstart,btnscore,btnabout,btnexit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        btnstart = (Button) findViewById(R.id.btnstart);
        btnscore = (Button) findViewById(R.id.btnscore);
        btnabout = (Button) findViewById(R.id.btnabout);
        btnexit = (Button) findViewById(R.id.btnexit);


        btnstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Game Started",Toast.LENGTH_LONG).show();

                Intent i = new Intent(getApplicationContext(),GameLuncher.class);
                startActivity(i);

            }
        });



    }



}
