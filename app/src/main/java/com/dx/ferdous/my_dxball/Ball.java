package com.dx.ferdous.my_dxball;

import android.widget.Toast;

/**
 * Created by ferdous on 9/6/17.
 */

public final class Ball {

    private static float x_coordinate=0;
    private static float y_coordinate=0;
    private static float radius=0;
    private static float ball_speed=1;

    private static float x_start=0;
    private static float y_start=0;

    private static float x_max=0;
    private static float y_max=0;

    private static boolean x_flag=true;
    private static boolean y_flag=true;


    Ball(){

        x_coordinate=0;
        y_coordinate=0;
        radius=30;
        x_start=0;
        y_start=0;
        x_max=0;
        y_max =0;
        ball_speed=1;
        x_flag=true;
        y_flag=true;


    }

    Ball(float x,float y,float rad,float x_m,float y_m,float sp){

        x_coordinate=x;
        y_coordinate=y;
        x_start=x;
        y_start=y;
        radius=rad;
        ball_speed=sp;
        x_max=x_m;
        y_max=y_m;
        x_flag=true;
        y_flag=true;

    }

    public static void ballMovement() {


        if(x_flag){

            x_coordinate+=ball_speed;

            if(x_coordinate >= x_max-radius){

                x_flag=false;
            }

        }else{

            x_coordinate-=ball_speed;

            if(x_coordinate <= radius ){

                x_flag=true;
            }
        }


        if(y_flag){

            y_coordinate+=ball_speed;

            if(collution()){

                y_flag=false;
            }

        }else{

            y_coordinate-=ball_speed;

            if(y_coordinate <= radius){

                y_flag=true;
            }
        }

        if(y_max+radius <= y_coordinate){

            x_coordinate=(Rectangle.get_rectleft()+Rectangle.get_rectright())/2;
            y_coordinate=y_max-Ball.get_ballradius()-Rectangle.get_height()-30;

            x_flag=true;
            y_flag=true;

        }


    }

    public static float get_ballx(){return x_coordinate;}

    public static float get_bally(){return y_coordinate;}

    public static float get_ballradius(){return radius;}

    public static boolean get_xflag(){return x_flag;}

    public static boolean get_yflag(){return y_flag;}





    public static void set_xflag(boolean item){x_flag=item;}

    public static void set_yflag(boolean item){y_flag=item;}

    public static void set_xmax(float x ){x_max=x;}

    public static void set_ymax(float y ){y_max=y;}

    public static void set_ballx(float x ){x_coordinate=x;}

    public static void set_ballx(){x_coordinate=x_max/2;}

    public static void set_bally(float y ){y_coordinate=y;}

    public static void set_bally(){y_coordinate=y_max-Rectangle.get_height()-5;}

    public static void set_ballradius(float r ){radius=r;}

    public static void set_ballspeed(float s ){ball_speed=s;}





    private static boolean collution(){


        if(x_coordinate+radius >= Rectangle.get_rectleft() && x_coordinate-radius <= Rectangle.get_rectright() && y_coordinate+radius ==Rectangle.get_recttop()){
          //  Toast.makeText(getContext(), "Collution", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;

    }





}
